const path = require('path');
const webpack = require('webpack');

var config = {
    context: __dirname + '/src',
    entry: {
      app: ['./app.js', './css/style.scss'],

    },
    output: {
        path: __dirname + '/dist',
        filename: '[name].bundle.js'
    },
    module: {
        rules: [
            {
                loader: "babel-loader",
                include: [path.resolve(__dirname, "src")],
                test: /\.js$/,
                query: {
                    plugins: ['transform-runtime'],
                    presets: ['es2015', 'stage-0'],
                }
            },
              // scss
             {
                test: /\.(sass|scss)$/,
                use: ['style-loader', 'css-loader', 'sass-loader'],
            }
        ]
    },
};

module.exports = config;
